from selenium import webdriver

from selenium.webdriver.common.by import By


#User_Case_2
driver = webdriver.Chrome(executable_path="F:\\Selenium_python_webdriver\\selenium_basic_program\\chromedriver.exe")
driver.implicitly_wait(10)
driver.maximize_window();
driver.get("http://automationpractice.com/index.php")
driver.find_element_by_xpath("//a[contains(text(),'Sign in')]").click()

#User login
driver.find_element_by_xpath("//input[@id='email']").send_keys("test#$%^124@test.com")
driver.find_element_by_xpath("//input[@id='passwd']").send_keys("test1234")
driver.find_element_by_xpath("//body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[2]/form[1]/div[1]/p[2]/button[1]/span[1]").click()

#User Scenario_2
driver.find_element_by_xpath("//input[@id='search_query_top']").send_keys("dresses")
driver.find_element_by_xpath("//header/div[3]/div[1]/div[1]/div[2]/form[1]/button[1]").click()

#To find result found is over 5
B=driver.find_elements_by_xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[2]/ul[1]/li")



print("Number of results which are more than 5 is : ")
print(len(B))

#Sort By Highest First
driver.find_element_by_xpath("//select[@id='selectProductSort']").send_keys("Price: Highest first")
driver.implicitly_wait(10)
#Sort By
driver.find_element_by_xpath("//select[@id='selectProductSort']").send_keys("In stock")
driver.implicitly_wait(10)

#Click on Blouse
driver.find_element_by_xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/ul[1]/li[3]/div[1]/h5[1]/a[1]").click()

driver.implicitly_wait(10)
#Print the Item data Compositions, styles and properties
Compositions = driver.find_element_by_xpath("//td[contains(text(),'Cotton')]")
print("Compositions: "+Compositions.text)

styles = driver.find_element_by_xpath("//td[contains(text(),'Casual')]")
print("styles: "+styles.text)

properties = driver.find_element_by_xpath("//td[contains(text(),'Short Sleeve')]")
print("properties: "+properties.text)

#signout
driver.find_element_by_xpath("//header/div[2]/div[1]/div[1]/nav[1]/div[2]/a[1]").click()

driver.quit()
